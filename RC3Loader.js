import * as THREE from "three";
import { onmessage } from "./RC3Loader.worker.js";

/**
 * THREE.js loader for .rc3 encoded models
 */
class RC3Loader {
  /**
   * Enable or disable logging
   * @param {boolean} logging True or false
   */
  constructor(logging) {
    this.timestamp = 0;
    this.logging = logging;

    this.getTexture = this.getTexture.bind(this);
    this.getBasicMaterial = this.getBasicMaterial.bind(this);
    this.getStandartMaterial = this.getStandartMaterial.bind(this);
    this.getShaderMaterial = this.getShaderMaterial.bind(this);
    this.messageHandler = this.messageHandler.bind(this);
  }

  /**
   * @callback OnLoadCallback
   * @param {object} Group THREE.js Group
   * @returns {void}
   */

  /**
   * Load file
   * @param {string} url Url of the file to load
   * @param {OnLoadCallback} callback Called when group is loaded
   * @param {string} materialType "basic" (default) | "standart" | "shader"
   */
  load(url, callback, materialType) {
    // build worker from blob and creating full url for it
    const workerCode = "onmessage = " + String(onmessage);

    const blob = new Blob([workerCode], {type: 'application/javascript'});
    const worker = new Worker(URL.createObjectURL(blob));

    const urlFull = new URL(url, window.location.href).href;
    this.url = urlFull;

    this.timestamp = Date.now();
    this.callback = callback;
    this.materialType = materialType || "basic";

    worker.onmessage = this.messageHandler;

    if (this.logging) {
      console.log(`RC3Loader: loading file "${url}"`);
    }

    worker.postMessage({
      url: this.url,
      logging: this.logging,
      timestamp: this.timestamp,
    });
  }

  /**
   * Directly parses passed data in a main thread synchronously
   * @param {string | ArrayBuffer} data Data to parse
   * @param {OnLoadCallback} callback Callback when data is parse
   * @param {string} materialType "basic" (default) | "standart" | "shader"
   * @param {string} dataType "ArrayBuffer" (default) | "base64" | "string"
   */
  parse(data, callback, materialType, dataType) {
    this.callback = callback;
    this.materialType = materialType || "basic";

    onmessage({data: {
      buffer: data,
      logging: this.logging,
      timestamp: this.timestamp,
      type: dataType || "ArrayBuffer"
    }}).then((result) => {
      this.messageHandler({
        data: result
      });
    }) 
  }
  /**
   * Handles response from web worker
   * @param {object} message Worker response.
   */
  messageHandler(message) {
    const data = message.data;
    const container = new THREE.Group();
    const materials = [];

    if (this.logging) {
      console.log(`RC3Loader: creating meshes`);
    }

    this.timestamp = Date.now();

    data.materials.forEach((m) => {
      let material;

      if (this.materialType === "basic") {
        material = this.getBasicMaterial(m);
      } else if (this.materialType === "standart") {
        material = this.getStandartMaterial(m);
      } else if (this.materialType === "shader") {
        material = this.getShaderMaterial(m);
      }
      
      materials.push(material);
    });

    data.geometries.forEach((geometry) => {
      const bufferGeometry = new THREE.BufferGeometry();

      if (geometry.vertices && geometry.vertices.length) {
        bufferGeometry.setAttribute(
          "position",
          new THREE.BufferAttribute(new Float32Array(geometry.vertices), 3)
        );
      }

      if (geometry.indices && geometry.indices.length) {
        bufferGeometry.setIndex(
          new THREE.BufferAttribute(new Uint32Array(geometry.indices), 1)
        );
      }

      if (geometry.colors && geometry.colors.length) {
        bufferGeometry.setAttribute(
          "color",
          new THREE.BufferAttribute(new Float32Array(geometry.colors), 3)
        );
      }

      if (geometry.normals && geometry.normals.length) {
        bufferGeometry.setAttribute(
          "normal",
          new THREE.BufferAttribute(new Float32Array(geometry.normals), 3)
        );
      } else {
        bufferGeometry.computeVertexNormals();
      }

      if (geometry.uvs && geometry.uvs.length) {
        bufferGeometry.setAttribute(
          "uv",
          new THREE.BufferAttribute(new Float32Array(geometry.uvs), 2)
        );
      }

      if (geometry.skinIndex && geometry.skinIndex.length) {
        bufferGeometry.setAttribute(
          "skinIndex",
          new THREE.BufferAttribute(new Uint16Array(geometry.skinIndex), 4)
        );
      }

      if (geometry.skinWeight && geometry.skinWeight.length) {
        bufferGeometry.setAttribute(
          "skinWeight",
          new THREE.BufferAttribute(new Float32Array(geometry.skinWeight), 4)
        );
      }

      let material;
      if (geometry.materialIndex === undefined || !materials[geometry.materialIndex]) {
        material = new THREE.MeshBasicMaterial();
      } else {
        material = materials[geometry.materialIndex];
      }

      const mesh = new THREE.Mesh(bufferGeometry, material);
      mesh.name = geometry.name;

      container.add(mesh);
    });

    if (this.logging) {
      console.log(
        `RC3Loader: finished creating meshes in ${
          Date.now() - this.timestamp
        }ms`
      );
    }

    this.callback(container);
  }

  /**
 * Load texture
 * @param {string} fileName Texture url 
 * @returns {object} Texture 
 */
  getTexture(fileName) {
    const baseUrl = this.url.substr(0, this.url.lastIndexOf("\/"));
    const texture = new THREE.TextureLoader().load(baseUrl + "/" + fileName, () => {
      texture.needsUpdate = true;
    });
    return texture;
  }

  /**
   * Build basic material from parameters
   * @param {object} parameters Material parameters.
   * @returns {object} TREE.MeshBasicMaterial 
   */
  getBasicMaterial(parameters) {
    const material = new THREE.MeshBasicMaterial();

    if (parameters.color !== undefined) {
      material.color = new THREE.Color(parameters.color.r, parameters.color.g, parameters.color.b);
    }
    if (parameters.opacity !== undefined) {
      material.opacity = parameters.opacity;
      material.transparent = true;
    }
    if (parameters.reflectivity !== undefined) { //todo
      material.reflectivity = parameters.reflectivity;
    }
    if (parameters.alphaMap !== undefined) {
      material.alphaMap = this.getTexture(parameters.alphaMap);
    }
    if (parameters.map !== undefined) {
      material.map = this.getTexture(parameters.map);
    }

    return material;
  }

  /**
   * Build standart material from parameters
   * @param {object} parameters Material parameters.
   * @returns {object} TREE.MeshStandartMaterial 
   */
  getStandartMaterial(parameters) {
    const material = new THREE.MeshStandardMaterial();

    if (parameters.color !== undefined) {
      material.color = new THREE.Color(parameters.color.r, parameters.color.g, parameters.color.b);
    }
    if (parameters.opacity !== undefined) {
      material.opacity = parameters.opacity;
      material.transparent = true;
    }
    if (parameters.emissive !== undefined) {
      material.emissive = new THREE.Color(parameters.emissive.r, parameters.emissive.g, parameters.emissive.b);
    }
    if (parameters.metalness !== undefined) { //todo
      material.metalness = parameters.metalness;
    }
    if (parameters.roughness !== undefined) { //todo
      material.roughness = parameters.metalness;
    }
    if (parameters.alphaMap !== undefined) {
      material.alphaMap = this.getTexture(parameters.alphaMap);
    }
    if (parameters.map !== undefined) {
      material.map = this.getTexture(parameters.map);
    }
    if (parameters.specularMap !== undefined) {
      material.specularMap = this.getTexture(parameters.specularMap);
    }
    if (parameters.bumpMap !== undefined) {
      material.bumpMap = this.getTexture(parameters.bumpMap);
    }
    if (parameters.emissiveMap !== undefined) {
      material.emissiveMap = this.getTexture(parameters.emissiveMap);
    }
    if (parameters.normalMap !== undefined) {
      material.normalMap = this.getTexture(parameters.normalMap);
    }

    return material;
  }

  /**
   * Build shader material from parameters
   * @param {object} parameters Material parameters.
   * @returns {object} TREE.ShaderMaterial 
   */
  getShaderMaterial(parameters) {
    const material = new THREE.ShaderMaterial();

    return material;
  }
}

export { RC3Loader };