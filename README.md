# RC3Loader
___
THREE.js loader for .rc3 encoded models.  
To load textures put them in the same folder as the .rc3 file  

## Instalation  
npm i three  
npm i @red-collar/rc3loader  

## Arguments  
### RC3Loader  
**logging**: boolean; Default: **false**. Enables logging  

### RC3Loader.load  
**src**: string; Path to the .rc3 file  
**callback**: Callback function, recieves a THREE.js group as an argument  
**materialType**: "basic" (default) | "standart"; Type of THREE.js material to use  

## Usage:  

```javaScript
import { RC3Loader } from "@red-collar/rc3loader";

const loader = new RC3Loader();
loader.load(src, (group) => {
    // do something
});
```

```javaScript
import { RC3Loader } from "rc3loader";

// Enable logging with true
const loader = new RC3Loader(true);
loader.load(src, (group) => {
}, "standart"); // Set material to MeshStandartMaterial by passing the "standart" string
```