export const onmessage = function (message) {
  let byteOffset = 0;
  let startTimestamp = message.data.timestamp;
  const loggingEnabled = message.data.logging;
  const type = message.data.type;

  /**
   * Load file
   * @param {string} url input file
   */
  const load = (url) => {
    fetch(url)
      .then((response) => response.arrayBuffer())
      .then((buffer) => parseArrayBuffer(buffer))
      .then((data) => postMessage(data));
  };

  /**
   * Converts string with binary data to arrayBuffer
   * @param {string} string
   */
  const binStringToBuffer = (byteString) => {
    const encoder = new TextEncoder();
    return encoder.encode(byteString).buffer;
  };

  /**
   * Converts dataURI to arrayBuffer
   * @param {string} string
   */
  const dataURItoBuffer = (dataURI) => {
    var byteString =  atob(dataURI);
    
    const buffer = new ArrayBuffer(byteString.length);
    const view = new Uint8Array(buffer);

    for (var i = 0; i < byteString.length; i++) {
      view[i] = byteString.charCodeAt(i);
    }
  
    return buffer;
  }

  /**
   * Reads array buffer and outputs geometry data
   * @param {ArrayBuffer} buffer data as ArrayBuffer
   * @returns {Promise} on complete promise
   */
  const parseArrayBuffer = (buffer) => {
    if (type === "string") {
      buffer = binStringToBuffer(buffer);
    } else if (type === "base64") {
      buffer = dataURItoBuffer(buffer);
    }

    if (loggingEnabled) {
      console.log(`RC3Loader: file loaded in ${Date.now() - startTimestamp}ms`);
      startTimestamp = Date.now();
    }

    let dataView = new DataView(buffer);

    const format = readAsString(byteOffset, 4, dataView);
    const version = readAsNumber(byteOffset, 2, dataView);

    if (loggingEnabled) {
      console.log(
        `RC3Loader: reading file, format ${format}, version: ${version}`
      );
    }

    let materials = [];
    if (version >= 2) {
      materials = readMaterials(dataView);
    }

    const numberOfGeometries = readAsNumber(byteOffset, 2, dataView);

    if (loggingEnabled) {
      console.log(`RC3Loader: geometries: ${numberOfGeometries}`);
    }

    const vertexCompress = readAsNumber(byteOffset, 1, dataView);
    const normalCompress = readAsNumber(byteOffset, 1, dataView);
    const colorCompress = readAsNumber(byteOffset, 1, dataView);

    if (loggingEnabled) {
      console.log(`RC3Loader: compression for vertices: ${vertexCompress}`);
      console.log(`RC3Loader: compression for normals: ${normalCompress}`);
      console.log(`RC3Loader: compression for colors: ${colorCompress}`);
    }

    const geometries = [];

    for (
      let geometryIndex = 0;
      geometryIndex < numberOfGeometries;
      geometryIndex++
    ) {
      const geometryNameLength = readAsNumber(byteOffset, 2, dataView);
      const geometryName = readAsString(
        byteOffset,
        geometryNameLength,
        dataView
      );

      let materialIndex = undefined;
      if (version >= 2) {
        materialIndex = readAsNumber(byteOffset, 2, dataView);
        if (materialIndex === 65535) {
          materialIndex = undefined;
        }
      }

      if (loggingEnabled) {
        console.log(
          `RC3Loader: reading geometry "${geometryName}", material index: ${materialIndex}`
        );
      }

      const numberOfVertices = readAsNumber(byteOffset, 4, dataView);

      if (loggingEnabled) {
        console.log(
          `RC3Loader: geometry "${geometryName}", vertices: ${numberOfVertices}`
        );
      }

      const hasPositions = !!readAsNumber(byteOffset, 1, dataView);
      const hasNormals = !!readAsNumber(byteOffset, 1, dataView);
      const hasColors = !!readAsNumber(byteOffset, 1, dataView);
      const hasTexCoords = !!readAsNumber(byteOffset, 1, dataView);

      let hasSecondTexCoords = false;
      let hasSkinning = false;

      if (version >= 2) {
        hasSecondTexCoords = !!readAsNumber(byteOffset, 1, dataView);
        hasSkinning = !!readAsNumber(byteOffset, 1, dataView);
        readAsNumber(byteOffset, 1, dataView); // empty
        readAsNumber(byteOffset, 1, dataView); // empty
      }

      if (loggingEnabled) {
        console.log(
          `RC3Loader: geometry "${geometryName}" has positions: ${hasPositions}`
        );
        console.log(
          `RC3Loader: geometry "${geometryName}" has normals: ${hasNormals}`
        );
        console.log(
          `RC3Loader: geometry "${geometryName}" has colors: ${hasColors}`
        );
        console.log(
          `RC3Loader: geometry "${geometryName}" has uv: ${hasTexCoords}`
        );
        console.log(
          `RC3Loader: geometry "${geometryName}" has skinning: ${hasSkinning}`
        );
      }

      const vertices = new Float32Array(
        hasPositions ? numberOfVertices * 3 : 0
      );
      const normals = new Float32Array(hasNormals ? numberOfVertices * 3 : 0);
      const colors = new Float32Array(hasColors ? numberOfVertices * 3 : 0);
      const uvs = new Float32Array(hasTexCoords ? numberOfVertices * 2 : 0);

      if (hasPositions) {
        if (vertexCompress > 0) {
          const origin = {
            x: readAsFloat(byteOffset, 4, dataView),
            y: readAsFloat(byteOffset, 4, dataView),
            z: readAsFloat(byteOffset, 4, dataView),
          };

          const size = {
            x: readAsFloat(byteOffset, 4, dataView),
            y: readAsFloat(byteOffset, 4, dataView),
            z: readAsFloat(byteOffset, 4, dataView),
          };

          if (loggingEnabled) {
            console.log(
              `RC3Loader: geometry "${geometryName}", origin: x:${origin.x}, y:${origin.y}, z:${origin.z}`
            );
            console.log(
              `RC3Loader: geometry "${geometryName}", size: x:${size.x}, y:${size.y}, z:${size.z}`
            );
          }

          for (let i = 0; i < numberOfVertices; i++) {
            if (vertexCompress === 1) {
              vertices[i * 3] =
                (readAsNumber(byteOffset, 2, dataView) / 65535) * size.x +
                origin.x; // x
              vertices[i * 3 + 1] =
                (readAsNumber(byteOffset, 2, dataView) / 65535) * size.y +
                origin.y; // y
              vertices[i * 3 + 2] =
                (readAsNumber(byteOffset, 2, dataView) / 65535) * size.z +
                origin.z; // z
            } else if (vertexCompress === 2) {
              vertices[i * 3] =
                (readAsNumber(byteOffset, 1, dataView) / 255) * size.x +
                origin.x; // x
              vertices[i * 3 + 1] =
                (readAsNumber(byteOffset, 1, dataView) / 255) * size.y +
                origin.y; // y
              vertices[i * 3 + 2] =
                (readAsNumber(byteOffset, 1, dataView) / 255) * size.z +
                origin.z; // z
            } else {
              if (loggingEnabled) {
                console.error(
                  `RC3Loader: unsupported compressin value for positions: ${vertexCompress}`
                );
              }
            }
          }
        } else {
          for (let i = 0; i < numberOfVertices; i++) {
            vertices[i * 3] = readAsFloat(byteOffset, 4, dataView); // x
            vertices[i * 3 + 1] = readAsFloat(byteOffset, 4, dataView); // y
            vertices[i * 3 + 2] = readAsFloat(byteOffset, 4, dataView); // z
          }
        }
      }

      if (hasNormals) {
        if (normalCompress > 0) {
          for (let i = 0; i < numberOfVertices; i++) {
            const lat =
              (readAsNumber(byteOffset, 1, dataView) * (Math.PI * 2)) / 255;
            const lng =
              (readAsNumber(byteOffset, 1, dataView) * (Math.PI * 2)) / 255;

            normals[i * 3] = Math.cos(lat) * Math.sin(lng); // x
            normals[i * 3 + 1] = Math.cos(lng); // y
            normals[i * 3 + 2] = Math.sin(lat) * Math.sin(lng); // z
          }
        } else {
          for (let i = 0; i < numberOfVertices; i++) {
            normals[i * 3] = readAsFloat(byteOffset, 4, dataView); // x
            normals[i * 3 + 1] = readAsFloat(byteOffset, 4, dataView); // y
            normals[i * 3 + 2] = readAsFloat(byteOffset, 4, dataView); // z
          }
        }
      }

      if (hasColors) {
        for (let i = 0; i < numberOfVertices; i++) {
          colors[i * 3] = readAsNumber(byteOffset, 1, dataView); // x
          colors[i * 3 + 1] = readAsNumber(byteOffset, 1, dataView); // y
          colors[i * 3 + 2] = readAsNumber(byteOffset, 1, dataView); // z
        }
      }

      if (hasTexCoords) {
        for (let i = 0; i < numberOfVertices; i++) {
          uvs[i * 2] = readAsFloat(byteOffset, 4, dataView); // x
          uvs[i * 2 + 1] = readAsFloat(byteOffset, 4, dataView); // y
        }
      }

      const numberOfIndices = readAsNumber(byteOffset, 4, dataView);
      const bytesPerIndex = readAsNumber(byteOffset, 1, dataView);

      const indices = new Uint32Array(numberOfIndices);

      if (loggingEnabled) {
        console.log(
          `RC3Loader: reading geometry "${geometryName}", indices: ${numberOfIndices}, bytes per index: ${bytesPerIndex}`
        );
      }

      for (let i = 0; i < numberOfIndices; i++) {
        indices[i] = readAsNumber(byteOffset, bytesPerIndex, dataView);
      }

      geometries.push({
        name: geometryName,
        indices: indices,
        vertices: vertices,
        normals: normals,
        colors: colors,
        uvs: uvs,
        materialIndex: materialIndex,
      });

      if (loggingEnabled) {
        console.log(`RC3Loader: finished reading geometry "${geometryName}"`);
      }
    }

    if (loggingEnabled) {
      console.log(
        `RC3Loader: finished reading file in ${Date.now() - startTimestamp}ms`
      );
    }

    return Promise.resolve({
      geometries: geometries,
      materials: materials,
    });
  };

  /**
   * Reads data about materials
   * @param {ArrayBuffer} dataView Binary data buffer
   * @returns Array of material data
   */
  const readMaterials = (dataView) => {
    const numberOfMaterials = readAsNumber(byteOffset, 2, dataView);
    const materials = [];

    if (loggingEnabled) {
      console.log(`RC3Loader: reading materials in file: ${numberOfMaterials}`);
    }

    for (let i = 0; i < numberOfMaterials; i++) {
      const material = {};

      const properyFlagsAsNumber = readAsNumber(byteOffset, 1, dataView);

      const properyFlags = (properyFlagsAsNumber >>> 0)
        .toString(2)
        .split("")
        .map((f) => parseInt(f));

      if (loggingEnabled) {
        console.log(
          `RC3Loader: material ${i} property flags: color ${properyFlags[0]} ambient ${properyFlags[1]} specular ${properyFlags[2]} emissive ${properyFlags[3]} opacity ${properyFlags[4]}`
        );
      }

      if (properyFlags[0]) {
        // Diffuse-color
        material.color = {
          r: readAsNumber(byteOffset, 1, dataView) / 255,
          g: readAsNumber(byteOffset, 1, dataView) / 255,
          b: readAsNumber(byteOffset, 1, dataView) / 255,
        };

        if (loggingEnabled) {
          console.log(
            `RC3Loader: material ${i} color: r ${material.color.r}, g ${material.color.g}, b ${material.color.b}`
          );
        }
      }

      if (properyFlags[1]) {
        // Ambient-color
        material.ambient = {
          r: readAsNumber(byteOffset, 1, dataView) / 255,
          g: readAsNumber(byteOffset, 1, dataView) / 255,
          b: readAsNumber(byteOffset, 1, dataView) / 255,
        };

        if (loggingEnabled) {
          console.log(
            `RC3Loader: material ${i} ambient: r ${material.ambient.r}, g ${material.ambient.g}, b ${material.ambient.b}`
          );
        }
      }

      if (properyFlags[2]) {
        // Specular-color
        material.specular = {
          r: readAsNumber(byteOffset, 1, dataView) / 255,
          g: readAsNumber(byteOffset, 1, dataView) / 255,
          b: readAsNumber(byteOffset, 1, dataView) / 255,
          e: readAsFloat(byteOffset, 4, dataView),
        };

        if (loggingEnabled) {
          console.log(
            `RC3Loader: material ${i} specular: r ${material.specular.r}, g ${material.specular.g}, b ${material.specular.b}, e ${material.specular.e}`
          );
        }
      }

      if (properyFlags[3]) {
        // Emissive-color
        material.emissive = {
          r: readAsNumber(byteOffset, 1, dataView) / 255,
          g: readAsNumber(byteOffset, 1, dataView) / 255,
          b: readAsNumber(byteOffset, 1, dataView) / 255,
        };

        if (loggingEnabled) {
          console.log(
            `RC3Loader: material ${i} emissive: r ${material.emissive.r}, g ${material.emissive.g}, b ${material.emissive.b}`
          );
        }
      }

      if (properyFlags[4]) {
        // Opacity
        material.opacity = readAsNumber(byteOffset, 1, dataView) / 255;

        if (loggingEnabled) {
          console.log(`RC3Loader: material ${i} opacity: ${material.opacity}`);
        }
      }

      const textureFlagsAsNumber = readAsNumber(byteOffset, 1, dataView);

      const textureFlags = (textureFlagsAsNumber >>> 0)
        .toString(2)
        .split("")
        .map((f) => parseInt(f));

      if (loggingEnabled) {
        console.log(
          `RC3Loader: material ${i} texture flags: map ${textureFlags[0]} specularMap ${textureFlags[1]} emissiveMap ${textureFlags[2]} normalMap ${textureFlags[3]} bumpMap ${textureFlags[4]} alphaMap ${textureFlags[5]}`
        );
      }

      if (textureFlags[0]) {
        // Diffuse
        material.map = readAsVariableString(byteOffset, 2, dataView);

        if (loggingEnabled) {
          console.log(`RC3Loader: material ${i} map: ${material.map}`);
        }
      }

      if (textureFlags[1]) {
        // Specular
        material.specularMap = readAsVariableString(byteOffset, 2, dataView);

        if (loggingEnabled) {
          console.log(
            `RC3Loader: material ${i} specularMap: ${material.specularMap}`
          );
        }
      }

      if (textureFlags[2]) {
        // Emissive
        material.emissiveMap = readAsVariableString(byteOffset, 2, dataView);

        if (loggingEnabled) {
          console.log(
            `RC3Loader: material ${i} emissiveMap: ${material.emissiveMap}`
          );
        }
      }

      if (textureFlags[3]) {
        // Normal
        material.normalMap = readAsVariableString(byteOffset, 2, dataView);

        if (loggingEnabled) {
          console.log(
            `RC3Loader: material ${i} normalMap: ${material.normalMap}`
          );
        }
      }

      if (textureFlags[4]) {
        // Bump
        material.bumpMap = readAsVariableString(byteOffset, 2, dataView);

        if (loggingEnabled) {
          console.log(`RC3Loader: material ${i} bumpMap: ${material.bumpMap}`);
        }
      }

      if (textureFlags[5]) {
        // Alpha
        material.alphaMap = readAsVariableString(byteOffset, 2, dataView);

        if (loggingEnabled) {
          console.log(
            `RC3Loader: material ${i} alphaMap: ${material.alphaMap}`
          );
        }
      }

      materials.push(material);
    }

    if (loggingEnabled) {
      console.log(`RC3Loader: finished reading materials`);
    }

    return materials;
  };

  const readBones = (dataView) => {
    return [];
  };

  /**
   * Reads data from array buffer as a string
   * @param {number} offset offset in bytes from the start of the file
   * @param {number} length lenght of the segment if bytes
   * @param {DataView} dataView DataView container of the input file
   * @returns
   */
  const readAsString = (offset, length, dataView) => {
    let out = "";

    for (let i = offset; i < offset + length; i++) {
      out += String.fromCharCode(dataView.getUint8(i));
    }

    byteOffset += length;

    return out;
  };

  /**
   * Reads string length from the first {length} bytes and then returns the string
   * @param {number} offset offset in bytes from the start of the file
   * @param {number} length lenght of the segment if bytes
   * @param {DataView} dataView DataView container of the input file
   * @returns
   */
  const readAsVariableString = (offset, length, dataView) => {
    const urlLength = readAsNumber(offset, length, dataView);
    return readAsString(byteOffset, urlLength, dataView);
  };

  /**
   * Reads data from array buffer as number
   * @param {number} offset offset in bytes from the start of the file
   * @param {number} length lenght of the segment if bytes
   * @param {DataView} dataView DataView container of the input file
   * @returns
   */
  const readAsNumber = (offset, length, dataView) => {
    let out;

    if (length === 1) {
      out = dataView.getUint8(offset);
    } else if (length === 2) {
      out = dataView.getUint16(offset, true);
    } else if (length === 4) {
      out = dataView.getInt32(offset, true);
    } else {
      console.error(`RC3Loader: unsupported byte length for Number: ${length}`);
    }

    byteOffset += length;

    return out;
  };

  /**
   * Reads data from array buffer as float
   * @param {number} offset offset in bytes from the start of the file
   * @param {number} length lenght of the segment if bytes
   * @param {DataView} dataView DataView container of the input file
   * @returns
   */
  const readAsFloat = (offset, length, dataView) => {
    let out;

    if (length === 4) {
      out = dataView.getFloat32(offset, true);
    } else if (length === 8) {
      out = dataView.getFloat64(offset, true);
    } else {
      console.error(`RC3Loader: unsupported byte length for Float: ${length}`);
    }

    byteOffset += length;

    return out;
  };

  if (message.data.url) {
    load(message.data.url);
  } else if (message.data.buffer) {
    return parseArrayBuffer(message.data.buffer);
  }
};