export type OnLoadCallback = (Group: object) => void;
/**
 * THREE.js loader for .rc3 encoded models
 */
export class RC3Loader {
    /**
     * Enable or disable logging
     * @param {boolean} logging True or false
     */
    constructor(logging?: boolean);
    timestamp: number;
    logging: boolean;
    /**
   * Load texture
   * @param {string} fileName Texture url
   * @returns {object} Texture
   */
    getTexture(fileName: string): object;
    /**
     * Build basic material from parameters
     * @param {object} parameters Material parameters.
     * @returns {object} TREE.MeshBasicMaterial
     */
    getBasicMaterial(parameters: object): object;
    /**
     * Build standart material from parameters
     * @param {object} parameters Material parameters.
     * @returns {object} TREE.MeshStandartMaterial
     */
    getStandartMaterial(parameters: object): object;
    /**
     * Build shader material from parameters
     * @param {object} parameters Material parameters.
     * @returns {object} TREE.ShaderMaterial
     */
    getShaderMaterial(parameters: object): object;
    /**
     * @callback OnLoadCallback
     * @param {object} Group THREE.js Group
     * @returns {void}
     */
    /**
     * Load file
     * @param {string} url Url of the file to load
     * @param {OnLoadCallback} callback Called when group is loaded
     * @param {string} materialType "basic" (default) | "standart" | "shader"
     */
    load(url: string, callback: OnLoadCallback, materialType: string): void;
    url: string;
    callback: OnLoadCallback;
    materialType?: string;
  /**
   * Directly parses passed data in a main thread synchronously
   * @param {string | ArrayBuffer} data Data to parse
   * @param {OnLoadCallback} callback Callback when data is parse
   * @param {string} materialType "basic" (default) | "standart" | "shader"
   * @param {string} dataType "ArrayBuffer" (default) | "base64" | "string"
   */
     parse(url: string, callback: OnLoadCallback, materialType: string, dataType: string): void;
     url: string;
     callback: OnLoadCallback;
     materialType?: string;
     dataType?: string;
     /**
      * Handles response from web worker
      * @param {object} message Worker response.
      */
    messageHandler(message: object): void;
}